import { Component, OnInit } from '@angular/core';
import {Recipe} from '../recipe.model';
@Component({
  selector: 'app-recipes-list',
  templateUrl: './recipes-list.component.html',
  styleUrls: ['./recipes-list.component.css']
})
export class RecipesListComponent implements OnInit {

  //"recipes" is the object that we call inside the ngFor directive
  recipes:Recipe[]=[
    new Recipe('Teramisu','Desert made with Esperesso',
    'https://upload.wikimedia.org/wikipedia/commons/thumb/f/fc/Tiramisu_with_blueberries_and_raspberries%2C_July_2011.jpg/1200px-Tiramisu_with_blueberries_and_raspberries%2C_July_2011.jpg'),
    new Recipe('Teramisu','Desert made with Esperesso',
    'https://upload.wikimedia.org/wikipedia/commons/thumb/f/fc/Tiramisu_with_blueberries_and_raspberries%2C_July_2011.jpg/1200px-Tiramisu_with_blueberries_and_raspberries%2C_July_2011.jpg')
  ];
  constructor() { }

  ngOnInit() {
  }

}
