export class Ingredient{
    //this is same as creating class variables and initializ them inside a constructor
    constructor(public name:string, public amount:number){}
}